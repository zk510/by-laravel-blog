<?php

use Faker\Generator as Faker;
use App\Models\Reply;
$factory->define(Reply::class, function (Faker $faker) {
    return [
        //
        'reply'=>$faker->sentence(),
        'mess_id'=>$faker->numberBetween(800,1000),
        'user_id'=>$faker->numberBetween(1,100),
        'ip'=>$faker->ipv4,
        'address'=>'中国'.'-中国'.'-'.$faker->address,
        'created_at'=>$faker->dateTimeThisYear('now','PRC')
    ];
});

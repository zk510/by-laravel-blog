<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Favorite;
use Faker\Generator as Faker;

$factory->define(Favorite::class, function (Faker $faker) {
    return [
        //
        'article_id'=>$faker->numberBetween(1,22),
        'user_id'=>$faker->numberBetween(1,100),
        'created_at'=>$faker->dateTimeThisYear('now','PRC')
    ];
});

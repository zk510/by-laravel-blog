<?php

use Faker\Generator as Faker;


$factory->define(App\Models\Message::class, function (Faker $faker) {
    return [
        'message'=>$faker->realText,
        'user_id'=>$faker->numberBetween(1,100),
        'article_id'=>$faker->numberBetween(0,22),
        'ip'=>$faker->ipv4,
        'address'=>'中国'.'-中国'.'-'.$faker->address,
        'created_at'=>$faker->dateTimeThisYear('now','PRC')
    ];
});

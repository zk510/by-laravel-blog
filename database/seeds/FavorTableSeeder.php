<?php

use Illuminate\Database\Seeder;
use App\Models\Favorite;
class FavorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Favorite::class, 1000)->create();
    }
}

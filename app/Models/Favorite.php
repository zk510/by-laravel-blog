<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Favorite extends Model
{
    use SoftDeletes;
    //
    //
    // 接受的字段
    protected $fillable = [
        'article_id', 'user_id','created_at'
    ];
    protected $table= 'favorite';
    // protected $dates = ['deleted_at'];
    // 表格隐藏的字段
    // protected $hidden = [
    //     ''
    // ];

}
